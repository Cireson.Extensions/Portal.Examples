var payload = {
	'parse': 'full',
	'channel': '#fun',
	'username': 'IT Monkey (bot)',
	'text': 'ID: ' + pageForm.viewModel.Id + 
		'\nTitle: ' + pageForm.viewModel.Title +
		'\nDescription: ' +	pageForm.viewModel.Description,
	'icon_emoji': ':monkey_face:'
}

$.ajax({
	url: 'https://hooks.slack.com/services/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', 
	type: 'POST',
	dataType: 'json', 
	data: {
		payload: JSON.stringify(payload)
	},
	error: function (data, responseText, error) {
		console.log(error);
	}
});