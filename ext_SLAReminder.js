function slaReminders () {
	var uid = session.user.Id;
	
	if($('#analystTable').length === 0) {
		$(".taskmenu").after('<div><h2>SLA Reminders</h2><table id="analystTable"><tr><th></th></tr></table></div>');
	};

	$.ajax({
		url: '/api/V3/WorkItem/GetGridWorkItemsByUser',
		async: true, //this is the default but showing that it can be turned off
		data: {
			userId: uid,
			isScoped: false,
			showActivities: false,
			showInactiveItems: false
		},
		type: "GET", //this particular portal API call is a GET type
		statusCode: {
			200: function (data) {
				var wiArray = data
				//debug console.log(wiArray);

				if(wiArray.length > 0) {
					var stale = [];
					

					for (var i = 0; i < wiArray.length; i++) {
						/* use this section for stale incidents by date
						var lmDate = wiArray[i].LastModified,
							staleDate = new Date(),
							daysOld = 7;
						staleDate.setDate(staleDate.getDate() - daysOld);
						var trigger = lmDate < staleDate;
						*/
						
						/* use this section to display only breached and warning state incidents */
						var trigger = ( wiArray[i].SLOStatus != null );
						
						/* this looks at the SLO Status to apply the proper icon */
						if ( trigger ) {
							var sloClass = null;
							if ( wiArray[i].SLOStatus == "Breached" ) {
								wiArray[i].sloClass = 'slo-icon fa fa-bell fa-lg text-danger';
							}
							else if ( wiArray[i].SLOStatus == "Warning" ) {
								wiArray[i].sloClass = 'slo-icon fa fa-exclamation-circle text-warning fa-lg'
							} 
							else { 
								wiArray[i].sloClass = 'slo-icon fa fa-clock-o text-primary fa-lg';
							};
							/* adds the incident to the array along with the icon information */
							stale.push(wiArray[i]);
						}
					};

					//debug console.log(stale);

					/* this runs through the data array and adds the first 5 returned results to the Related KBs table */
					for (var i = 0; i < stale.length && i < 5; i++) {
						$('#analystTable > tbody > tr')
							.last()
							.after('<tr><td class="'+stale[i].sloClass+'"></td><td nowrap><a href="/Incident/Edit/'+stale[i].Id+'">'+stale[i].Title.substring(0,17)+'...'+'</a></td></tr>');
						$('#analystTable > tbody > tr > td').css({
															'padding': '3px',
															'font-size': '14px'
														});
					};
				};
			}
		},
		error: function (jqXHR, textStatus, errorThrown) {
			//debug console.log(errorThrown);
			//debug console.log(textStatus);
		}
	});
}