/* ----------------------------------------------- */
/* ----- Add Totals To Grouped By Grid Views ----- */
/* ----------------------------------------------- */
// Tested for: Cireson Portal v5.0.8
$(document).ready(function () {
	if (session.user.AssetManager === 0 && session.user.IsAdmin === false) {
		return;
	}
	
	var url = window.location.href;
	if(url.indexOf("/View/") === -1){ // Loosely verify we are on a page with a gridview. Not really reliable, but it helps...
		return;
	}
	
	var groupedByTotal = 0
	//The navigation node doesn't load immediately. Get the main div that definitely exists.
	var mainPageNode = document.getElementById('main_wrapper');
	
	// create an observer instance
	var observer = new MutationObserver(function(mutations) {
		//The page changed. See if our title exists. If it does, then our gridview should also exist.
		var titleElement = $(".page_title"); //The title always exists. If this page has a gridview, then it also exists as soon as the title does.
		
        if (titleElement.length > 0) { //An element with class of page_title exists.
			var gridElement = $('[data-role=grid]') // Get the grid object
			if (gridElement.length > 0) {
				AddNumberTextOnGridDataSourceChange(gridElement);
			}
			
			//We are done observing.
            observer.disconnect();
        }
		
	});
	
	// configure the observer and start the instance.
	var observerConfig = { attributes: true, childList: true, subtree: true, characterData: true };
	observer.observe(mainPageNode, observerConfig);
	
});

function AddNumberTextOnGridDataSourceChange(gridElement) {
	
	var kendoGridElement = gridElement.data('kendoGrid'); //...as a kendo widget
	
	//Whenever the data changes, update the totals if applicable.
	kendoGridElement.bind("dataBound", AddTotalsToGroupedByGridViews); 
	//Note to self. the dataBound fires after the rows are returned, and dataBinding fires before. 
	
}

function AddTotalsToGroupedByGridViews() {
	//Get all of our grouped-by filters at the top of the grid. We will only calculate if there is one.
	var groupedByFilters = $(".k-grouping-header .k-link span.k-icon");
	if (groupedByFilters.length !== 1) {
		console.log("Grid View totals counter found " + groupedByFilters.length + " GroupBy filters. Exiting.");
		return;
	}

	//Get all of our grouped elements on this grid view.
	var groupedByTrs = $(".grid-container tr.k-grouping-row");
	if (groupedByTrs.length === 0) {
		console.log("Grid View found no rows that match the grouped by criteria. Exiting.");
		return;
	}

	currentGroupCounter = 0
	for (var i = 0; i < groupedByTrs.length; i++) {
		//The TRs aren't parent/child, rather all siblings. So find when the next group by column starts based on class.
		var thisGroupedByTr = $(groupedByTrs[i]);
		var allTrsWithThisGroupBy = thisGroupedByTr.nextUntil("tr.k-grouping-row"); //Get all rows between now and the next group by header.
		groupByCount = allTrsWithThisGroupBy.length
		
		//Add the count to our groupby RT.
		//console.log("Appending " + groupByCount + " to " + thisGroupedByTr);
		firstPElement = thisGroupedByTr.find("p").first();
		firstPElement.append(" (" + groupByCount + ")");
		
	}
}
/* ----------------------------------------------- */
/* --- End Add Totals To Grouped By Grid Views --- */
/* ----------------------------------------------- */
