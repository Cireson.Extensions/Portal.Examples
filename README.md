# Portal Extension Examples

This shows a number of basic examples for how to extend the portal. This readme will briefly highlight the available examples and their limitations.

### Related KB Extension

This looks at the title of an incident and then parses the title and does a search against the portal to find KBs that might be related.

It works on the idea of _relevance_ showing the top 5 articles, with the most relevent first.