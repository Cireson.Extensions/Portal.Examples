app.custom.formTasks.add('Incident', 'Resolve Incident', function (formObj, viewModel) {
	function ResolveIncident (incident) {
		console.log("Setting incident to 'Resolved'...");
		incident.Status.Id = "2b8830b6-59f0-f574-9c2a-f4b4682f1681"
		incident.ResolutionDescription = "This Incident was resolved by the resolve incident task."
		incident.ResolutionCategory.Id = "c5f6ada9-a0df-01d6-7087-6b8500ca6c2b";   
		incident.ResolvedDate = new Date().toISOString().split(".")[0];
		
		console.log("Incident set to 'Resolved', refreshing header and awaiting save..");
		$('#statusname').html('Resolved');
	}
	
	ResolveIncident(viewModel);
});